package de.hska.iwii.gui.solution;

import de.hska.iwii.gui.drawing.DrawingListener;
import de.hska.iwii.gui.drawing.MainWindow;
import javafx.scene.Node;
import javafx.scene.paint.Color;

import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * User: Seb
 * Date: 12.07.13
 * Time: 14:01
 * To change this template use File | Settings | File Templates.
 */
public class Ereignisbehandler implements DrawingListener {
	
	private Canvas panel = new Canvas();
	
	public Ereignisbehandler(Canvas panel){
		this.panel = panel;
	}
	
	@Override
	public void startCreateFigure(String figureType, double xPos, double yPos) {
		//Je nachdem, was für ein Button geklickt ist die entsprechende Figur erstellen
		//und dann in die Zeichenfläche einfügen
		if (figureType.equals("ellipse")){
			Ellipse newFigure = new Ellipse();
			//newFigure.setRadiusX(100);
			//newFigure.setRadiusY(100);
			newFigure.setScaleX(100);
			newFigure.setScaleY(100);
			newFigure.setCenterX(xPos);
			newFigure.setCenterY(yPos);
			newFigure.setFill(Color.BLUE);
		}
		else if(figureType.equals("rectangle")){
			Rectangle newFigure = new Rectangle();
			newFigure.setWidth(100);
			newFigure.setHeight(100);
			newFigure.setX(xPos);
			newFigure.setY(yPos);
			newFigure.setFill(Color.PINK);
			
			// Das Viereck in das Panel einfügen
			panel.getChildren().add(newFigure);

		}
		else{
			Line newFigure = new Line();
			newFigure.setStartX(xPos);
			newFigure.setStartY(yPos);
			newFigure.setEndY(59);
			newFigure.setEndX(59);
			newFigure.setStroke(Color.RED);
			panel.getChildren().add(newFigure);
		}
	}

	@Override
	public void startMoveFigure(Node node, double xPos, double yPos) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void workCreateFigure(double xPos, double yPos) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void workMoveFigure(Node node, double xPos, double yPos) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void endCreateFigure(double xPos, double yPos) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void endMoveFigure(Node node, double xPos, double yPos) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void selectFigure(Node node, double xPos, double yPos, boolean shiftPressed) {
		//To change body of implemented methods use File | Settings | File Templates.
		System.out.print("Nanana!!");
	}

	@Override
	public void rotate(Node node, double angle) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void translate(Node node, double deltaX, double deltaY) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void zoom(Node node, double zoomFactor) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void deleteFigures() {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void copyFigures() {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void pasteFigures() {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void moveSelectedFiguresToTop() {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void moveSelectedFiguresToBottom() {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void moveSelectedFiguresDown() {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void moveSelectedFiguresUp() {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void groupFigures() {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void ungroupFigures() {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public int getSelectedFiguresCount() {
		return 0;  //To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public int getFiguresInClipboardCount() {
		return 0;  //To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public boolean isGroupSelected() {
		return false;  //To change body of implemented methods use File | Settings | File Templates.
	}
}
