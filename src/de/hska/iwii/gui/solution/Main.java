package de.hska.iwii.gui.solution;

import de.hska.iwii.gui.drawing.DrawingListener;
import de.hska.iwii.gui.drawing.MainWindow;
import javafx.application.Application;
import javafx.scene.paint.Color;

/**
 * Created with IntelliJ IDEA.
 * User: Seb
 * Date: 12.07.13
 * Time: 13:41
 * To change this template use File | Settings | File Templates.
 */
public class Main {

	public static void main (String[] args){

		//Ein neues Canvas Objekt erstellen und diesem die Minimal- und Maximalgröße mitgeben
		//Dann wird dieses Objekt dem MainWindow übergeben
		Canvas panel = new Canvas();
		panel.setMinSize(600,600);
		panel.setMaxSize(600,600);
		MainWindow.setMainPanel(panel);

		//Ein Rechteck erstellen und dieses dem Panel übergeben (Warum da das getChildren()
		//davor muss weiß ich nicht, hat aber das Internet gesagt:
		// http://docs.oracle.com/javafx/2/api/javafx/scene/layout/Pane.html
		Rectangle rectangle = new Rectangle();
		rectangle.setHeight(120);
		rectangle.setWidth(120);
		rectangle.setFill(Color.AQUA);
		panel.getChildren().add(rectangle);

		//Nach dem gleichen Schema eine Linie einfügen
		Line line = new Line();
		line.setStartX(101);
		line.setStartY(0);
		line.setEndY(59);
		line.setEndX(59);
		line.setStroke(Color.RED);
		panel.getChildren().add(line);


		//Ein neues Ereignisbehandler Objekt erstellen und dieses dem MainWindow übergeben
		Ereignisbehandler listener = new Ereignisbehandler(panel);
		MainWindow.setDrawingListener(listener);


		//Den ganzen Shit starten
		Application.launch(MainWindow.class, args);
	}

}
